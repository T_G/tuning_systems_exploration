A script that defines a python class to represent a random walker over just intonation frequencies. Steps can go to nearest neighbors only and tend to keep the herd of walkers together.


To create an animated gif from pdfs use:
convert -delay 25 -loop 0 *.pdf Im_walkin.gif
