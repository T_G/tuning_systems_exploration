# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 21:49:30 2017

@author: torsten
"""

import numpy as np


log_2_primes=np.log2([3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271])

class walker:
    def __init__(self,dim_tuning_system,root_freq=440.,initial_position=[]):
        self.root_freq=root_freq
        self.dim_tuning_system=dim_tuning_system
        if not initial_position:
            self.position=np.zeros(dim_tuning_system,dtype=int)
        else: self.position=initial_position
        self.calculate_frequency()
    
    def calculate_frequency(self):
        y=np.sum(self.position * log_2_primes[:self.dim_tuning_system])
        self.frequency=np.power(2,y-np.floor(y))*self.root_freq
        return
    
    def next_step(self,position_other_walkers=[],diffusion=1.):
        #potential next positions are all with hamming distance of 1 or 0
        potential_positions=[list(self.position)]
        for i in range(self.dim_tuning_system):
            potential_positions.append(list(self.position))
            potential_positions[-1][i]+=1
            potential_positions.append(list(self.position))
            potential_positions[-1][i]-=1
        
        #compute probabilites of going to different positions
        mean_distances_to_other_walkers=[]
        if position_other_walkers:
            for potential_position in potential_positions:
                distances_to_other_walkers=[]
                for position_other_walker in position_other_walkers:
                    distances_to_other_walkers.append(np.sum(np.abs(np.subtract(position_other_walker,potential_position))))
                mean_distances_to_other_walkers.append(np.mean(distances_to_other_walkers))
            
            diff_dist_potential_pos=np.array(mean_distances_to_other_walkers)-mean_distances_to_other_walkers[0]
        else: diff_dist_potential_pos=np.zeros(len(potential_positions))
        
        probas_pot_position=[]
        for diff_dist in diff_dist_potential_pos:
            if diff_dist<0:
                probas_pot_position.append(diffusion-diff_dist)
            else: probas_pot_position.append(diffusion)
        probas_pot_position=np.array(probas_pot_position)/np.sum(probas_pot_position,dtype=float)
        
        #make random weighted decision about position
        pos_pick=np.random.rand()
        cum_proba=0
        for pot_pos_ind,proba_pot_position in enumerate(probas_pot_position):
            cum_proba+=proba_pot_position
            if pos_pick<=cum_proba:
                break
        
        self.position = potential_positions[pot_pos_ind]
        self.calculate_frequency()
        
        return
    
boing=walker(2,1,[0,0])
for i in range(10):
    boing.next_step()
    print boing.position, boing.frequency



import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('ticks')

diffusion=1.
boings=[]
for i in range(3):
    boings.append(walker(2,1,[2*(i-1),2*(i-1)]))


plot_dims=[5,5]
for step in range(100):
    positions=[boing.position for boing in boings]
    freqs=[boing.frequency for boing in boings]
    for boing_ind,boing in enumerate(boings):
        boing.next_step(positions[:boing_ind]+positions[boing_ind+1:],diffusion)
    
    plt.figure(figsize=[5,5])
    plt.scatter(*zip(*positions),c=sns.color_palette('deep',len(positions)),s=1000,marker='s')
    for pos,freq in zip(positions,freqs):
        plt.text(pos[0]-0.5,pos[1]-0.15,np.round(freq,2))
    plt.axis([-plot_dims[0],plot_dims[0],-plot_dims[1],plot_dims[1]])
    plt.xlabel('exponents of 3')
    plt.ylabel('exponents of 5')
    plt.title('Just walking')
    plt.tight_layout()
    plt.savefig('walking_step_'+str(step).zfill(3)+'.pdf')
    #print positions
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
